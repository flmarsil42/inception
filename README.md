# inception

Le projet consiste à mettre en place une mini-infrastructure de différents services en suivant des règles spécifiques.

- Les différents services sont :

  - Un serveur web (Nginx) accessible via https.
  - Un serveur wordpress + phpfpm
  - Un serveur mariadb
  - Un serveur adminer + phpfpm
  - Un serveur cache redis

`Prérequis pour lancer le projet : Docker et docker-compose`

### Construire et lancer le projet

1. Télécharger / Cloner le repo

```
git clone https://gitlab.com/flmarsil42/inception.git
```

2. Ajouter "127.0.0.1 flmarsil.42.fr" dans le fichier /etc/hosts de la machine hote (voir fichier .env pour modifier la variable d'environnement)

3. `cd` dans le dossier racine, et lancer `make install`. Cela va construire toutes les images et lancer les containers.
